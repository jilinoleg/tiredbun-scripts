#!/bin/bash
# Mini TUI client for Bitwarden cli
# Depends - coreutils, findutils, bw, jq, fzf, wl-clipboard
# Author: tiredbun
# ----------------------------
if [ -z ${BW_SESSION+x} ]; then
BW_SESSION=$(bw unlock "$BW_PASSWORD" --raw)
export BW_SESSION
fi

CONTENTS=$(bw list items | jq .[].name | fzf | xargs -IITEM bw list items --search ITEM)

CHOICE=$(echo "Copy password
Display Login and Password
Store in file" | fzf)

if [[ $CHOICE == *"Copy password"* ]]; then
  tmp=$(echo "$CONTENTS" | jq ".[].login.password")
  tmp="${tmp%\"}"
  tmp="${tmp#\"}" 
  wl-copy "$tmp"
  #this method is used to only delete first and last quote
fi
if [[ $CHOICE == *"Display Login and Password"* ]]; then
  jq ".[].login.username, .[].login.password" <<< "$CONTENTS"
fi
if [[ $CHOICE == *"Store in file"* ]]; then
  tmp=$(jq .[].name <<< "$CONTENTS")
  touch "${tmp//\"}.json" 
  echo "$CONTENTS" | jq . >> "${tmp//\"}.json" 
  echo "JSON data stored in ${tmp//\"}.json"
  #unintended behavior - deletes all quotes
fi
