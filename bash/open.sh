#!/bin/bash
# open with xargs script
# Depends - bash, findutils, fzf, xdg-portal
# Author: tiredbun
# ----------------------------
find . | fzf -q "$1" --select-1 | xargs -Ifile xdg-open "file"
