### bw.sh - simple Bitwarden TUI written in BASH 
**![bw.sh](bash/bw.sh)**
 
**Dependencies** - `bash`, `coreutils`, `findutils`, `bw` (bitwarden cli), `jq`, `fzf`, `wl-clipboard` (if you use X11 ignore and change wl-copy to cli clipboard manager you use)

**Usage**
- Unlock vault by inputting password
- Search item name in fzf and press enter
- Choose what to do with it:
    - Copy password into a clipboard
    - Display username and password in terminal
    - Write item data into a JSON file.

### open.sh - simple TUI/CLI file opener written in bash 
**![open.sh](bash/open.sh)**
 
**Dependencies** - `bash`, `findutils`, `fzf`, `xdg-portal` (+desktop enviroment (DE) that can use it and DE-specific xdg-portal-\<de\> package)

**Usage**
- Use command with argument of desired filename or file path relative to current one (search is fuzzy and recursive)
    - If there is only one matching file found it will automatically open it in what you chose as default in your GUI apps
    - If there is none or more than one it will let you use fzf to adjust search and choose what you want